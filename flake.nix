{
  description = "NixOS configuration using flakes.";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    nur.url = "github:nix-community/NUR";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, nixos-hardware, home-manager, nur }: {
    nixosConfigurations =
      let
        nixpkgs-config = {
          nixpkgs = {
            config.allowUnfree = true;
            overlays = [
              nur.overlay
            ];
          };
        };
        hm-user-module = { user, hostname }: ({ config, pkgs, ... }: {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            users.${user} = import (./user + "/${user}/hosts/${hostname}.nix");
          };
        });
      in
      {
        crow = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            nixpkgs-config
            nixos-hardware.nixosModules.lenovo-thinkpad-t420
            nixos-hardware.nixosModules.common-pc-laptop-ssd
            ./hosts/crow/hardware.nix
            ./hosts/crow/configuration.nix
            ./modules/audio/musnix/default.nix
            home-manager.nixosModules.home-manager
            (hm-user-module { user = "lyla"; hostname = "crow"; })
          ];
        };

        ferret = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            nixpkgs-config
            nixos-hardware.nixosModules.common-cpu-intel
            nixos-hardware.nixosModules.common-pc
            nixos-hardware.nixosModules.common-pc-ssd
            ./hosts/ferret/hardware.nix
            ./hosts/ferret/configuration.nix
            home-manager.nixosModules.home-manager
            (hm-user-module { user = "lyla"; hostname = "ferret"; })
          ];
        };
      };
  };
}
