{ config, pkgs, ... }:

{
  imports = [
    ../../modules/locale.nix
    ../../modules/nix.nix
    ../../user/lyla/default.nix
    ../../modules/x/fonts.nix
    ../../modules/x/xfce.nix
    ../../modules/avahi.nix
    ../../modules/openssh.nix
  ];

  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sda";
      useOSProber = true;
    };
    plymouth.enable = true;
    cleanTmpDir = true;
    kernelPackages = pkgs.linuxPackages_latest;
  };

  nix.distributedBuilds = pkgs.lib.mkForce false;

  security = {
    sudo = {
      enable = true;
      wheelNeedsPassword = false;
    };
  };

  networking = {
    hostName = "ferret";
    networkmanager.enable = true;
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "es";
  };

  services.xserver.layout = "es";

  sound.enable = true;
  hardware = {
    pulseaudio.enable = true;
    opengl = {
      enable = true;
      driSupport = true;
      extraPackages = with pkgs; [
        amdvlk
      ];
    };
  };

  users.mutableUsers = false;
  system.stateVersion = "21.03"; # Did you read the comment?
}
