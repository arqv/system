{ config, pkgs, ... }:

{
  imports = [
    ../../modules/locale.nix
    ../../modules/nix.nix
    ../../user/lyla/default.nix
    ../../modules/x/fonts.nix
    ../../modules/x/xfce.nix
    ../../modules/avahi.nix
    ../../modules/audio/default.nix
  ];

  boot = {
    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sda";
    };
    plymouth.enable = true;
    cleanTmpDir = true;
    kernelPackages = pkgs.linuxPackages_latest;
  };

  security = {
    sudo = {
      enable = true;
      wheelNeedsPassword = false;
    };
  };

  networking = {
    hostName = "crow";
    networkmanager.enable = true;
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "la-latin1";
  };

  environment = {
    homeBinInPath = true;
    systemPackages = with pkgs; [ xclip wget usbutils ];
  };

  programs = {
    adb.enable = true;
    tmux.enable = true;
    slock.enable = true;
    fish.enable = true;
  };

  services = {
    blueman.enable = true;
    xserver = {
      displayManager.autoLogin = {
        enable = true;
        user = "lyla";
      };
      layout = "latam";
      libinput.enable = true;
    };
  };

  sound.enable = true;
  hardware = {
    bluetooth.enable = true;
    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
    };
    opengl.enable = true;
    steam-hardware.enable = true;
  };

  users.mutableUsers = false;
  system.stateVersion = "21.03";
}
