{
  services.openssh = {
    enable = true;
    ports = [ 6969 ];
    permitRootLogin = "prohibit-password";
  };
}
