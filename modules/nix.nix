{ pkgs, ... }:
{
  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = ca-references nix-command flakes
      builders-use-substitutes = true
    '';
    trustedUsers = [ "root" "lyla" ];
    distributedBuilds = true;
    buildMachines = [
      {
        hostName = "ferret.local";
        supportedFeatures = [ "big-parallel" "benchmark" ];
        maxJobs = 16;
        sshKey = "/root/.ssh/ferret";
        system = "x86_64-linux";
      }

      #{
      #  hostName = "eu.nixbuild.net";
      #  supportedFeatures = [ "big-parallel" "benchmark" ];
      #  maxJobs = 100;
      #  sshKey = "/root/.ssh/nixbuild";
      #  system = "x86_64-linux";
      #}
    ];
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };

    binaryCaches = [
      "https://hydra.iohk.io"
    ];
    binaryCachePublicKeys = [
      "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
    ];
  };
  nixpkgs.config = { allowUnfree = true; };
}
