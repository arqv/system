{ config, pkgs, ... }:

{
  musnix = {
    enable = true;
    alsaSeq.enable = true;
    # kernel = {
    #   optimize = true;
    #   realtime = true;
    #   packages = pkgs.linuxPackages_latest_rt;
    # };
  };

  services = {
    jack = {
      jackd.enable = true;
      alsa.enable = false;
      loopback.enable = true;
    };
  };
}
