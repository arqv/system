{ pkgs, lib, ... }:

{
  # font configuration
  fonts = {
    fonts = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      corefonts
      inter
      agave hack-font creep
    ];
    fontconfig = {
      enable = true;
      subpixel.rgba = "none";
      defaultFonts = with lib; {
        sansSerif = mkForce [ "Inter" "Noto Sans" ];
        serif = mkForce [ "Noto Serif" ];
        monospace = mkForce [ "Agave" ];
      };
    };
  };
}
