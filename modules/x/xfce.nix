{ config, pkgs, ... }:

{
  services = {
    xserver = {
      enable = true;
      displayManager.lightdm = {
        enable = true;
      };
      desktopManager.xterm.enable = false;
      desktopManager.xfce = {
        enable = true;
        # TODO: not working on nixpkgs.
        # thunarPlugins = with pkgs.xfce; [
        #   thunar-volman
        #   thunar-archive-plugin
        # ];
      };
    };
    picom = {
      enable = true;
      shadow = false;
      menuOpacity = 0.9;
      inactiveOpacity = 0.95;
      backend = "glx";
      experimentalBackends = true;
    };
    redshift.enable = true;
  };
}
