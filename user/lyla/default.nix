{ config, pkgs, ... }:

{
  users.users.lyla = {
    isNormalUser = true;
    description = "Lyla Bravo";
    shell = pkgs.fish;
    hashedPassword =
      "$6$Qba86K44LFx$KNFKJwHozJe4KgJADlRJae7VinowqLwcwRXaIXSEDkhSnZh5nekhKjJzcxNHj4znZiUO3OQOQTYR.JPTKqjc7.";
    extraGroups = [ "wheel" "networkmanager" "jackaudio" "audio" "video" "adbusers" "input" ];
  };
}
