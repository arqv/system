{ config, pkgs, ... }:

{
  # machine-agnostic modules (non-GUI)
  imports = [
    ./modules/fish.nix
    ./modules/tmux.nix
    ./modules/neovim.nix
    ./modules/vis.nix
    ./modules/git.nix
  ];

  news.display = "silent";
  home = {
    username = "lyla";
    homeDirectory = "/home/lyla";
    stateVersion = "20.09";
    extraOutputsToInstall = [ "doc" ];
    packages = with pkgs; [
      neofetch unzip
      mosh
      exa
      fd
      ripgrep
      pandoc
      git-crypt
      dhall
      dhall-json
      dhall-lsp-server
      signify
      rlwrap
      rnix-lsp
      nixpkgs-fmt
      niv
      cachix
      manix
    ];
  };

  programs = {
    home-manager.enable = true;
    htop.enable = true;
    fzf.enable = true;
    jq.enable = true;
    direnv = {
      enable = true;
      enableNixDirenvIntegration = true;
      stdlib = ''
        : ''${XDG_CACHE_HOME:=$HOME/.cache}
        declare -A direnv_layout_dirs
        direnv_layout_dir() {
          echo "''${direnv_layout_dirs[$PWD]:=$(
            echo -n "$XDG_CACHE_HOME"/direnv/layouts/
            echo -n "$PWD" | shasum | cut -d ' ' -f 1
          )}"
        }
      '';
    };
    bat.enable = true;
  };

  xdg = {
    userDirs = {
      enable = true;
      desktop = "$HOME/usr";
      documents = "$HOME/usr/doc";
      download = "$HOME/usr/dl";
      music = "$HOME/usr/mus";
      pictures = "$HOME/usr/vis";
      videos = "$HOME/usr/vis";
      publicShare = "$HOME/.etc";
      templates = "$HOME/.etc";
    };
  };
}
