{ pkgs, ... }:

{
  home.packages = with pkgs; [
    libreoffice
    soulseekqt
    transmission-gtk
    discord
    obs-studio
    olive-editor
    krita
    darkice
    steam-run-native
  ];

  programs = {
    zathura.enable = true;
  };
  services = {
    flameshot.enable = true;
  };
}
