{
  programs.alacritty = {
    enable = true;
    settings = {
      window = {
        padding = {
          x = 0;
          y = 0;
        };
      };
      font = {
        normal = { family = "Hack"; };
        size = 8;
      };
      colors = {
        primary = {
          background = "0x121212";
          foreground = "0xd0d0d0";
        };
        normal = {
          black = "0x4e4e4e";
          red = "0xd68787";
          green = "0x5f865f";
          yellow = "0xd8af5f";
          blue = "0x85add4";
          magenta = "0xd7afaf";
          cyan = "0x87afaf";
          white = "0xd0d0d0";
        };
        bright = {
          black = "0x626262";
          red = "0xd75f87";
          green = "0x87af87";
          yellow = "0xffd787";
          blue = "0xadd4fb";
          magenta = "0xffafaf";
          cyan = "0x87d7d7";
          white = "0xe4e4e4";
        };
      };
    };
  };
}
