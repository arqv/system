{ pkgs, ... }:

{
  home.packages = with pkgs; [ luakit ];
  xdg.configFile."luakit/userconf.lua".source = ./luakit/userconf.lua;
}
