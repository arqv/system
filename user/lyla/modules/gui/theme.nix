{ pkgs, ... }:

{
  home.packages = with pkgs; [
    elementary-xfce-icon-theme
    vanilla-dmz
  ];

  gtk = {
    enable = true;
    theme = {
      name = "Adementary Dark";
      package = pkgs.adementary-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
  };
}
