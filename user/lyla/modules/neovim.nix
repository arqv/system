{ config, pkgs, ... }:

{
  home.sessionVariables."EDITOR" = "nvim";
  programs.neovim = {
    enable = true;
    withPython3 = true;
    extraConfig = builtins.readFile ./neovim/vimrc;
    plugins =
      let
        parinfer-rust = pkgs.kakounePlugins.parinfer-rust;
        janet-vim = pkgs.vimUtils.buildVimPlugin {
          name = "janet.vim";
          src = pkgs.fetchFromGitHub {
            owner = "janet-lang";
            repo = "janet.vim";
            rev = "1d616c0169138a017f245c104565bd25b079c8f0";
            sha256 = "1MNyUIvgl3EHBXPdPvvaYS89y80g+GenQwhBYLVJBzI=";
          };
          buildInputs = [ pkgs.janet ];
        };
        vim-tidal = pkgs.vimUtils.buildVimPlugin {
          name = "vim-tidal";
          src = pkgs.fetchFromGitHub {
            owner = "tidalcycles";
            repo = "vim-tidal";
            rev = "32614c3c5982ca00861cedbf3fa5a6515d9bb16b";
            sha256 = "+6GqLpwvuXsyuNUq/8v3WSeZAscjTjXoci0uoNCKQjw=";
          };

          # The Makefile tries to install binaries to /usr/local/bin,
          # however, since the plugin depends on them being on PATH,
          # we install them imperatively in another folder.
          # TODO: install them declaratively
          configurePhase = "rm Makefile";
        };
      in
      with pkgs.vimPlugins; [
        parinfer-rust
        fzf-vim
        haskell-vim
        indentLine
        vim-tidal
        janet-vim
        ir_black
        emmet-vim
        vim-polyglot
        vimsence
        LanguageClient-neovim
        # ale
        deoplete-nvim
        nerdcommenter
        seoul256-vim
        lightline-vim
        vim-orgmode
      ];
  };
}
