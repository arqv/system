{ pkgs, ... }:
{
  imports = [
    ../home.nix
    ../modules/gui/alacritty.nix
    ../modules/gui/firefox.nix
    ../modules/gui/luakit.nix
    ../modules/gui/theme.nix
    ../modules/gui/misc.nix
  ];
  home.packages = with pkgs; [
    zoom-us
    freemind
    supercollider
    qjackctl
    vcv-rack
    audacity
  ];
}
