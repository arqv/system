{
  imports = [
    ../home.nix
    ../modules/gui/alacritty.nix
    ../modules/gui/firefox.nix
    ../modules/gui/theme.nix
    ../modules/gui/misc.nix
  ];
  programs.alacritty.settings.font.size = 3;
}
